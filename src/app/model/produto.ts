import { Categoria } from './categoria';

export class Produto {
    id: number;
    nome: string;
    valor: number;
    categoria: string;

    private setCategoria(value) {
        let index = Object.values(Categoria)
        .findIndex(v => v == value);
        if (index > 0) {
            let key = Object.keys(Categoria)[index];
            this.categoria = key;
        } else {
            this.categoria = value;
        }
    }

    constructor(produto?: Produto) {
        if (produto) {
            this.id = produto.id;
            this.setCategoria(produto.categoria);
            this.nome = produto.nome;
            this.valor = produto.valor;
        }
    }

}
