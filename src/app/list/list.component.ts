import { Component, OnInit } from '@angular/core';
import { ProdutoService } from '../service/produto.service';
import { Produto } from '../model/produto';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public produtos: Array<Produto>;

  constructor(
    private produtoService: ProdutoService
  ) { }

  ngOnInit(): void {
    this.listAll();
  }

  private listAll() {
    this.produtoService
    .listAll()
    .subscribe(response => {
      this.produtos = response;
    });
  }

  public excluir(id: number) {
    this.produtoService
    .deleteById(id)
    .subscribe(() => {
      this.listAll();
    })
  }

}
