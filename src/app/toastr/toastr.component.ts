import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-toastr',
  templateUrl: './toastr.component.html',
  styleUrls: ['./toastr.component.css']
})
export class ToastrComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.alertService
    .subscribe()
    .subscribe(alrt => {
      if (alrt) {
        if (alrt.type === 'error') {
          this.toastr.error(alrt.message, 'Erro!')
        } else {
          this.toastr.success(alrt.message, 'OK!');
        }
      }
    })
  }

}
