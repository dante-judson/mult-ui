import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private message = new BehaviorSubject<{type:string,message:string}>(undefined);


  constructor() { }

  public subscribe(): Observable<{type:string,message:string}> {
    return this.message.asObservable();
  } 

  public sendMessage(message: string, type: 'error'|'success'):void {
    this.message.next({message, type});
  }


}
