import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";

import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Produto } from '../model/produto';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  private baseUrl = `${environment.protocol}://${environment.host}:${environment.port}/produto`

  constructor(
    private http: HttpClient
  ) { }

  public listAll(): Observable<Array<Produto>> {
    return this.http.get<Array<Produto>>(this.baseUrl);
  }

  public findById(id: number): Observable<Produto> {
    return this.http.get<Produto>(`${this.baseUrl}/${id}`);
  }

  public deleteById(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${id}`);
  }

  public add(produto: Produto): Observable<Produto> {
    return this.http.post<Produto>(`${this.baseUrl}`,produto);
  }

  public edit(produto: Produto): Observable<Produto> {
    return this.http.put<Produto>(`${this.baseUrl}`,produto);
  }
}
