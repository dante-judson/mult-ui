import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormComponent } from "./form/form.component";
import { ListComponent } from "./list/list.component";


const routes: Routes = [
    { path: '', component: ListComponent },
    { path: 'consultar/:id', component: FormComponent },
    { path: 'editar/:id', component: FormComponent },
    { path: 'adicionar', component: FormComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppModuleRouting { }