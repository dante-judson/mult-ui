import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { AppModuleRouting } from "./app.module.routing";
import { FormsModule } from "@angular/forms";
import { CustomFormsModule } from "ngx-custom-validators";
import { ToastrComponent } from './toastr/toastr.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorInterceptor } from './interceptor/error.interceptor';
import { AlertService } from './service/alert.service';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';



@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    FormComponent,
    ToastrComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppModuleRouting,
    HttpClientModule,
    FormsModule,
    CustomFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    SweetAlert2Module.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
      deps: [AlertService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
