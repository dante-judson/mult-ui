import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProdutoService } from '../service/produto.service';
import { Produto } from '../model/produto';
import { Categoria } from '../model/categoria';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public formMode: string;
  public produto: Produto = new Produto();
  public categorias = Object.values(Categoria);

  constructor(
    private route: ActivatedRoute,
    private produtoSerivce: ProdutoService,
    private router: Router,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.setupForm()
  }

  private setupForm():void {
    this.getFormModeFromUrl();
    if (this.formMode !== 'adicionar') {
      this.getProdutoFromUrl();
    }
  }

  private getFormModeFromUrl():void {
    this.route.url
    .subscribe(url => {
      this.formMode = url[0].path;
    })
    
  }

  private getProdutoFromUrl():void {
    this.route.paramMap
    .subscribe(paramMap => {
      let id = Number(paramMap.get('id'));
      this.produtoSerivce
      .findById(id)
      .subscribe(response => {
        this.produto = new Produto(response);        
      })
    })
  }

  public submitForm(){
    console.log(this.produto);
    
    if (this.formMode === 'adicionar') {
      this.adicinaProduto(this.produto);
    } else if (this.formMode === 'editar') {
      this.editaProduto(this.produto);
    }
  }

  private adicinaProduto(produto: Produto) {
    this.produtoSerivce.add(produto)
    .subscribe(() => {
      this.alertService.sendMessage('Produto adicionado com sucesso!','success');
      this.router.navigate(['']);
    })
  }

  private editaProduto(produto: Produto) {
    this.produtoSerivce.edit(produto)
    .subscribe(() => {
      this.alertService.sendMessage('Produto editado com sucesso!','success');
      this.router.navigate(['']);
    })
  }


}
